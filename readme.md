<h1 align="center">Welcome to gestversion 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.1-blue.svg?cacheSeconds=2592000" />
  <a href="https://gitlab.com/games-geeks/gestversion/-/blob/master/readme.md" target="_blank">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" />
  </a>
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
  <a href="https://twitter.com/gamesngeeks" target="_blank">
    <img alt="Twitter: gamesngeeks" src="https://img.shields.io/twitter/follow/gamesngeeks.svg?style=social" />
  </a>
</p>

> GestVersion est un petit site permettant de vérifier la mise à jour de nouvelles versions pour certaines applications.

## Prérequis

 * PHP 8.0
 * Composer
 * Symfony CLI
 * nodejs et npm

Vous pouvez vérifier les pré-requis (sauf Docker et Docker-compose) avec la commande suivante (de la CLI Symfony):
```bash
symfony check:recquirements
```

## Installation

Pour l'installation, vous aurez besoin de [composer](https://getcomposer.org/download/) et [npm](https://www.npmjs.com/get-npm)


```bash
composer update
npm install --force
npm run build
composer req symfony/apache-pack
```

### Creation de la bdd

Pour se connecter, il faut créer un fichier .env.local et y mettre les informations de connexion

```bash
# DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.db"
# DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7"
#DATABASE_URL="postgresql://db_user:db_password@127.0.0.1:5432/db_name?serverVersion=13&charset=utf8"
```

Ensuite on peut lancer la création de la base
```bash
php bin/console doctrine:schema:create
```

## Author

👤 **Denny**

* Website: games-geeks.fr
* Twitter: [@gamesngeeks](https://twitter.com/gamesngeeks)
* Github: [@games-geeks](https://github.com/games-geeks)

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
