<?php

namespace App\Entity;

use App\Repository\SiteRepository;
use App\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SiteRepository::class)
 * @ORM\Table(name="sites")
 * @ORM\HasLifecycleCallbacks
 */
class Site
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $installedVersion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastVersion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $recherche;

    /**
     * @ORM\Column(type="integer")
     */
    private $champs;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $balise;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $actionSeparateur;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $actionChamp;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $estNational;





    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getInstalledVersion(): ?string
    {
        return $this->installedVersion;
    }

    public function setInstalledVersion(string $installedVersion): self
    {
        $this->installedVersion
            = $installedVersion;

        return $this;
    }

    public function getLastVersion(): ?string
    {
        return $this->lastVersion;
    }

    public function setLastVersion(?string $lastVersion): self
    {
        $this->lastVersion = $lastVersion;

        return $this;
    }

    public function getRecherche(): ?string
    {
        return $this->recherche;
    }

    public function setRecherche(?string $recherche): self
    {
        $this->recherche = $recherche;

        return $this;
    }

    public function getChamps(): ?int
    {
        return $this->champs;
    }

    public function setChamps(int $champs): self
    {
        $this->champs = $champs;

        return $this;
    }

    public function getBalise(): ?string
    {
        return $this->balise;
    }

    public function setBalise(?string $balise): self
    {
        $this->balise = $balise;

        return $this;
    }

    public function getActionSeparateur(): ?string
    {
        return $this->actionSeparateur;
    }

    public function setActionSeparateur(?string $actionSeparateur): self
    {
        $this->actionSeparateur = $actionSeparateur;

        return $this;
    }

    public function getActionChamp(): ?int
    {
        return $this->actionChamp;
    }

    public function setActionChamp(?int $actionChamp): self
    {
        $this->actionChamp = $actionChamp;

        return $this;
    }

    public function getEstNational(): ?bool
    {
        return $this->estNational;
    }

    public function setEstNational(?bool $estNational): self
    {
        $this->estNational = $estNational;

        return $this;
    }
}
