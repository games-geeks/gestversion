<?php

namespace App\Form;

use App\Entity\Site;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SiteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => 'Nom de l\'application', 'attr' => ['autofocus' => true]])
            ->add('url', TextType::class, ['label' => 'URL de l\'application'])
            ->add('installedVersion', TextType::class, ['label' => 'Numéro de la version Installée'])
            ->add('estNational', CheckboxType::class, ['label' => 'Application Nationale?', 'required' => false,])
            ->add('recherche')
            ->add('champs')
            ->add('balise')
            ->add('actionSeparateur')
            ->add('actionChamp');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Site::class,
        ]);
    }
}
