<?php

namespace App\Service;

use App\Entity\Site;
use voku\helper\HtmlDomParser;
use Symfony\Component\Mime\Email;
use App\Repository\SiteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class MailService
{


    public static function sendMail(MailerInterface $mailer, string $application, string $to)
    {

        $email = (new Email())
            ->from('gestVersion@assurance-maladie.fr')
            ->to($to)
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->replyTo('fabien@example.com')
            //->priority(Email::PRIORITY_HIGH)
            ->subject('Une mise à jour est disponible pour l\'application ' . $application . '!')
            ->text('Sending emails is fun again!')
            ->html('<p>See Twig integration for better HTML integration!</p>');

        $mailer->send($email);
    }
}
