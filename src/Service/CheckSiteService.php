<?php

namespace App\Service;

use App\Entity\Site;
use voku\helper\HtmlDomParser;
use App\Repository\SiteRepository;
use Doctrine\ORM\EntityManagerInterface;

class CheckSiteService
{

    public static function checkNotePad(): string
    {

        $dom = HtmlDomParser::file_get_html("https://notepad-plus-plus.org/");
        $version = $dom->find('p.library-desc')->plaintext;
        $tabVersion[] = explode(' ', $version[0]);
        return trim($tabVersion[0][2]);

        //dd($dom->find('p.library-desc'));
        // foreach ($dom->find('p.library-desc') as $e) {
        //     echo $e->plaintext . '<br>';
        // }
    }
    public static function checkEclipse(): string
    {
        $dom = HtmlDomParser::file_get_html("https://www.eclipse.org/downloads/");
        $version = $dom->find('div.downloads-eclipse-installer-alert')->plaintext;
        $tabVersion[] = explode(' ', $version[0]);
        return trim($tabVersion[0][3]);
    }

    public static function checkOracle(): string
    {
        $dom = HtmlDomParser::file_get_html("https://www.oracle.com/tools/downloads/sqldev-downloads.html");
        $version = $dom->find('div.cc01w1 ')->plaintext;
        $tabVersion[] = explode(' ', $version[0]);
        return trim($tabVersion[0][2]);
    }

    public static function checkPHP(): string
    {
        $dom = HtmlDomParser::file_get_html("https://www.php.net/downloads.php");
        $version = $dom->find('h3.title')->plaintext;
        $tabVersion[] = explode(' ', $version[0]);
        return trim($tabVersion[0][2]);
    }

    public static function checkVersion(Site $site, EntityManagerInterface $entityManager)
    {
        $dom = HtmlDomParser::file_get_html($site->getUrl());
        if ($site->getBalise() == 'plaintext') {
            $version = $dom->find($site->getRecherche())->plaintext;
        } elseif ($site->getBalise() == 'innertext') {
            $version = $dom->find($site->getRecherche())->innertext;
        } else {
            $version = $dom->find($site->getRecherche())->outertext;
        }
        $tabVersion[] = explode(' ', $version[0]);
        $version = trim($tabVersion[0][$site->getChamps()]);
        $version = preg_replace("/&#(\d{2,5});/", "-", $version);
        $version = str_replace("\"", "", $version);
        if ($site->getActionSeparateur()) {
            $tabVersionSup[] = explode($site->getActionSeparateur(), $version);
            $version = trim($tabVersionSup[0][$site->getActionChamp()]);
        }
        $site->setLastVersion(htmlspecialchars_decode($version));
        //$entityManager->persist($site);
        $entityManager->flush();
        return $site->getLastVersion();
    }
}
