<?php

namespace App\Command;

use App\Service\MailService;
use App\Service\CheckSiteService;
use App\Repository\SiteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class CheckSiteCommand extends Command
{
    protected static $defaultName = 'version:checkSite';

    private $em;
    private $repo;
    private $mailer;
    private $params;

    public function __construct(
        EntityManagerInterface $entityManager,
        SiteRepository $siteRepository,
        MailerInterface $mailer,
        ContainerBagInterface $params
    ) {

        $this->em = $entityManager;
        $this->repo = $siteRepository;
        $this->mailer = $mailer;
        $this->params = $params;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Lancement de la vérification des versions');
        // ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
        // ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $sites = $this->repo->findAll();
        $table = new Table($output);

        $table->setHeaderTitle('Applications')
            ->setHeaders(['Application', 'Version installée', 'Dernière Version', 'Mise à jour à effectuer']);
        // ->setRows([
        //     ['Java Language Features', '978-1-4842-3347-4', 'Kishori Sharan', 'Apress'],
        //     ['Python Testing with pytest', '978-1-68-050-240-4', 'Brian Okken', 'The Pragmatic Programmers'],
        //     ['Deep Learning with Python', '978-1-61729-443-3', 'Francois Chollet', 'Manning'],
        //     ['Laravel up & Running', '978-1-491-93698-5', 'Matt Stauffer', 'O\'Reilly'],
        //     ['Sams Teach Yourself TCP/IP', '978-0-672-33789-5', 'Joe Casad', 'SAMS']
        // ]);



        $outputStyleRed = new OutputFormatterStyle('red');
        $output->getFormatter()->setStyle('redt', $outputStyleRed);
        $outputStyleGreen = new OutputFormatterStyle('green');
        $output->getFormatter()->setStyle('greent', $outputStyleGreen);



        foreach ($sites as $site) {
            //     $io->info($site->getName());
            //     //on ne vérifie que si on a l'information de recherche
            if ($site->getRecherche()) {
                $version = CheckSiteService::checkVersion($site, $this->em);
                $table->addRow(
                    [
                        $site->getName(), $site->getInstalledVersion(), $site->getLastVersion(),
                        ($site->getInstalledVersion() != $site->getLastVersion() ?
                            '<redt>KO</redt>' : '<greent>OK</greent>')
                    ]
                );

                if ($site->getInstalledVersion() != $site->getLastVersion()) {
                    //      $io->warning("L'application " . $site->getName() . "
                    //est disponible dans la version : " . $site->getLastVersion());
                    MailService::sendMail($this->mailer, $site->getName(), $this->params->get('mail_to'));
                    //  } else
                    //      $io->info("L'application " . $site->getName() . " est à jour en version : "
                    // . $site->getInstalledVersion());
                }
            }
        }

        $table->render();
        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return Command::SUCCESS;
    }
}
